import { MlevelFrontPage } from './app.po';

describe('mlevel-front App', function() {
  let page: MlevelFrontPage;

  beforeEach(() => {
    page = new MlevelFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
