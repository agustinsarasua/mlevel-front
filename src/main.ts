import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppComponent, environment } from './app/';
import { HTTP_PROVIDERS } from '@angular/http';
import { APP_ROUTER_PROVIDERS } from './app/app.routes';
import {provideForms, disableDeprecatedForms} from "@angular/forms";
import {FIREBASE_PROVIDERS,
  defaultFirebase,
  AngularFire,
  AuthMethods,
  AuthProviders,
  firebaseAuthConfig} from 'angularfire2';

if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent, [
  HTTP_PROVIDERS,
  APP_ROUTER_PROVIDERS,
  disableDeprecatedForms(),
  provideForms(),
  { provide: LocationStrategy, useClass: HashLocationStrategy },
  FIREBASE_PROVIDERS,
  defaultFirebase({
    apiKey: "AIzaSyAn8mo_s8l6jqM5wtqff_Qvf0FTVlHHBC4",
    authDomain: "pimba-backend.firebaseapp.com",
    databaseURL: "https://pimba-backend.firebaseio.com",
    storageBucket: "pimba-backend.appspot.com",
  }),
  firebaseAuthConfig({
    provider: AuthProviders.Facebook,
    method: AuthMethods.Popup,
    remember: 'default',
    scope: ['public_profile','email']
  })
])
.catch(err => console.error(err));
