import { Component, OnInit } from '@angular/core';
import { Router, ROUTER_DIRECTIVES }  from '@angular/router';
import {MdToolbar} from '@angular2-material/toolbar';
import {MdButton} from '@angular2-material/button';
import {MD_SIDENAV_DIRECTIVES} from '@angular2-material/sidenav';
import {MD_LIST_DIRECTIVES} from '@angular2-material/list';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';
import {LandingComponent} from "./landing/landing.component";
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { SearchFbPageComponent } from './home/components/search-fb-page/search-fb-page.component';
import { FacebookService } from "./services/facebook.service";
import { PageService } from "./services/page.service";
import { RestService } from "./services/rest.service";
import { AuthService } from "./services/auth.service";

declare var FB: any;

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  directives: [
    MdToolbar,
    MdButton,
    MD_SIDENAV_DIRECTIVES,
    MD_LIST_DIRECTIVES,
    MdIcon,
    ROUTER_DIRECTIVES,
    LandingComponent,
    SearchFbPageComponent
  ],
  providers: [MdIconRegistry, FacebookService, PageService, RestService],
  styleUrls: ['app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'Pimba';
  views: Object[] = [
    {
      name: "My Account",
      description: "Edit my account information",
      icon: "assignment_ind"
    },
    {
      name: "Events",
      description: "Your Pimba events",
      icon: "event"
    },
    {
      name: "Pages",
      description: "Facebook Connected Pages",
      icon: "view_carousel"
    }
  ];

  constructor(public af: AngularFire, private router: Router, public authService: AuthService) {
    this.af.auth.subscribe(auth => this.authenPimba(auth));
  }

  ngOnInit():any {
    FB.init({
      appId      : '1709613959262846', // Facebook App ID
      status     : true,  // check Facebook Login status
      cookie     : true,  // enable cookies to allow Parse to access the session
      xfbml      : true,  // initialize Facebook social plugins on the page
      version    : 'v2.6' // point to the latest Facebook Graph API version
    });
  }

  authenPimba(auth):any{
    if(auth != null) {
      localStorage.setItem("pimbaUserId", auth.uid);
      localStorage.setItem("pimbaToken", auth.auth.bd);
      this.authService.login();
      this.router.navigate(['/home']);
    }else{
      this.router.navigate(['/welcome']);
    }
  }

  login(){
    this.af.auth.login().then(data => {
      // alert(data);
    }, error => {
      // alert(error);
    });
  }

  logout(){
    localStorage.clear();
    this.authService.logout();
    this.af.auth.logout();
    this.router.navigate(['/welcome']);
  }

  facebookSearch(query){
    this.router.navigate(['/search', { type: 'page', q: query }]);
  }
}
