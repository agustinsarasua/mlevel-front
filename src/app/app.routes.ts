import { provideRouter, RouterConfig } from '@angular/router';
import { LandingComponent } from "./landing/landing.component";
import { HomeComponent } from "./home/home.component";
import { AuthGuard } from "./auth.guard"
import { AuthService } from "./services/auth.service"
import { AppComponent } from "./app.component"
import { SearchComponent } from "./search/search.component"

export const routes: RouterConfig = [
  { path: '', component: AppComponent},
  { path: 'welcome', component: LandingComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'search', component: SearchComponent, canActivate: [AuthGuard] }
]

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
  AuthGuard, AuthService
];
