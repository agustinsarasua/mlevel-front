/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { PageService } from './page.service';

describe('Page Service', () => {
  beforeEachProviders(() => [PageService]);

  it('should ...',
      inject([PageService], (service: PageService) => {
    expect(service).toBeTruthy();
  }));
});
