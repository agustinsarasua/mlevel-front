import { Injectable , OnInit } from '@angular/core';
import {Account} from "./../model/facebook/account";
import {Permission} from "./../model/facebook/permission";

declare var FB: any;

@Injectable()
export class FacebookService implements OnInit {

  constructor() {}

  ngOnInit():any {
    FB.init({
      appId      : '1709613959262846', // Facebook App ID
      status     : true,  // check Facebook Login status
      cookie     : true,  // enable cookies to allow Parse to access the session
      xfbml      : true,  // initialize Facebook social plugins on the page
      version    : 'v2.6' // point to the latest Facebook Graph API version
    });
  }

  public loadUserPermissions(accessToken) {
    return new Promise<Permission[]>(resolve =>
      FB.api('me/permissions', {
        access_token: accessToken
      }, (response) => {
        let result: Array<Permission> = [];
        if (response && !response.error) {
          /* handle the result */
          response.data.forEach((permission) => {
            let perm = new Permission(permission);
            result.push(perm);
          });
          resolve(result);
        }
      }));
  }

  public loadAccounts(accessToken) {
    return new Promise<Account[]>(resolve =>
      FB.api('me/accounts', {
        access_token: accessToken,
        fields: 'id,about,access_token,category,category_list,description,fan_count,genre,is_published,link,location,name,talking_about_count,website,checkins,country_page_likes,cover',
        limit: '20',
        is_promotable: true,
        summary: 'total_count'
      }, (response) => {
        let result: Array<Account> = [];
        if (response && !response.error) {
          /* handle the result */
          response.data.forEach((account) => {
            let fbAccount = new Account(account);
            result.push(fbAccount);
          });
          resolve(result);
        }
      }));
  }

  public searchPages(pageName, accessToken) {
    return new Promise<Account[]>(resolve =>
      FB.api('search', {
        access_token: accessToken,
        limit: '10',
        fields: 'id,about,access_token,category,category_list,description,fan_count,genre,is_published,link,location,name,talking_about_count,website,checkins,country_page_likes,cover',
        type: 'page',
        q: pageName
      }, (response) => {
        let result: Array<Account> = [];
        if (response && !response.error) {
          /* handle the result */
          response.data.forEach((account) => {
            let fbAccount = new Account(account);
            result.push(fbAccount);
          });
          resolve(result);
        }
      }));
  }


  // public loadFacebookEvents(userId: String) {
  //   return new Promise<FacebookEvent[]>(resolve =>
  //     FB.api('/me/events', {
  //       access_token: localStorage.getItem('token')
  //     }, (response) => {
  //       let result: Array<FacebookEvent> = [];
  //       if (response && !response.error) {
  //         /* handle the result */
  //         response.data.forEach((event) => {
  //           let fbEvent = new FacebookEvent(event);
  //           result.push(fbEvent);
  //         });
  //         resolve(result);
  //       }
  //     }));
  // }
  //
  // public loadEventCover(eventId: String){
  //   FB.api('/' + eventId, {
  //     access_token: localStorage.getItem('token'),
  //     fields: 'cover'
  //   }, (response) => {
  //     if (response && !response.error) {
  //       /* handle the result */
  //       return response;
  //     }
  //   });
  // }
}
