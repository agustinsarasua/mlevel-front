import {Injectable} from "@angular/core";
import {Http, Response, Headers} from '@angular/http';
import {Observable} from "rxjs/Observable";
import {Account} from "./../model/facebook/account";
import { RestService } from "./rest.service";
import { environment } from './../environment';

@Injectable()
export class PageService {

  // private hostUrl: string = 'https://pimba-backend.appspot.com/_ah/api/';
  private authHeader: string ='X-PimbaAuth';

  constructor(private http: Http, private restService: RestService) { }

  public saveFacebookPage (page: Account): Promise<string> {
    let body = JSON.stringify(page, (k,v) => { if(v == null) { return undefined; } else { return v; } } );
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.restService.doPost("facebook/v1/page", body, headers, true);
  }

  public sharePage(id: string): Promise<string> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.restService.doPost("facebook/v1/pages/"+id+"/share", "", headers, true);
  }

  public loadUserPages(): Promise<Account[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });

    let pimbaToken = localStorage.getItem("pimbaToken");
    headers.append(this.authHeader, pimbaToken)

    return this.http.get(environment.hostUrl + "facebook/v1/pages", { headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  extractData(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Bad response status: ' + res.status);
    }
    let body = res.json();
    return body.items || { };
  }

  handleError (error: any) {
    let errMsg = error.message || 'Server error';
    return Observable.throw(errMsg);
  }
}
