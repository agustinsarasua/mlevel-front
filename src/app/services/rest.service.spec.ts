/* tslint:disable:no-unused-variable */

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';
import { RestService } from './rest.service';

describe('Rest Service', () => {
  beforeEachProviders(() => [RestService]);

  it('should ...',
      inject([RestService], (service: RestService) => {
    expect(service).toBeTruthy();
  }));
});
