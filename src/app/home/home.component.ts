import { Component } from '@angular/core';
import { FacebookService } from "./../services/facebook.service";
import { PageService } from "./../services/page.service";
import { Account } from "./../model/facebook/account";
import { Permission } from "./../model/facebook/permission";
import { MdToolbar } from '@angular2-material/toolbar';
import { MdButton } from '@angular2-material/button';
import { MD_SIDENAV_DIRECTIVES } from '@angular2-material/sidenav';
import { MD_LIST_DIRECTIVES } from '@angular2-material/list';
import { MD_PROGRESS_CIRCLE_DIRECTIVES } from '@angular2-material/progress-circle';
import { MdIcon, MdIconRegistry } from '@angular2-material/icon';
import { ROUTER_DIRECTIVES }  from '@angular/router';
import { CardItemAComponent } from "./../abstract/components/card-item-a/card-item-a.component";
import { AccountCardComponent } from "./components/account-card/account-card.component";
import { SearchFbPageComponent } from './components/search-fb-page/search-fb-page.component';

declare var FB: any;

@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
  directives: [
    MdButton,
    MD_LIST_DIRECTIVES,
    MD_PROGRESS_CIRCLE_DIRECTIVES,
    MdIcon,
    ROUTER_DIRECTIVES,
    CardItemAComponent,
    AccountCardComponent,
    SearchFbPageComponent
  ],
  providers: [MdIconRegistry],
})
export class HomeComponent {

  fbAccounts: Account[] = [];
  userAccounts: Account[] = [];
  

  isLoading: boolean = false;
  permGranted: boolean = false;

  constructor(private _fbService: FacebookService, private _pageService: PageService) {
    this.isLoading = true;
    FB.getLoginStatus((response) => {
      this.statusChangeCallback(response);
    });
  }

  askForAccountsPermission(){
    FB.login((response) => {
      // handle the response
      if (response.status === 'connected') {
        // Logged into your app and Facebook.
        // console.log(response.authResponse.accessToken);
      } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
      } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
      }
    }, {scope: 'pages_show_list'});
  }

  statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
        localStorage.setItem("facebookAccessToken", response.authResponse.accessToken);
        this._fbService.loadUserPermissions(response.authResponse.accessToken)
          .then(permissions => {
              for (var item of permissions) {
                if(item.permission == 'pages_show_list' && item.status == 'granted'){
                  this.permGranted = true;
                }
              }
              if(this.permGranted){
                this._pageService.loadUserPages().then(pages => this.setUserAccounts(pages, response.authResponse.accessToken));
              } else {
                this.askForAccountsPermission();
              }
          });
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
    }
  }

  setUserAccounts(pages: Account[], accessToken) {
    this.userAccounts = pages;
    this._fbService.loadAccounts(accessToken).then(accounts => this.setFacebookAccounts(accounts));
  }

  setFacebookAccounts(accounts: Account[]){
    for (var fbItem of accounts) {
      var alreadyExists: boolean = false;
      for (var userItem of this.userAccounts) {
        if (fbItem.facebookId == userItem.facebookId){
          alreadyExists = true;
        }
      }
      if(!alreadyExists){
        this.fbAccounts.push(fbItem);
      }
    }
    this.isLoading = false;
  }

  sharePage(account, share: boolean){
    if(account.id != null) {
      account.share = share;
      this._pageService.sharePage(account.id).then(data => console.log(data), error => console.log(error))
    } else {
      if(share){
        this._pageService.saveFacebookPage(account)
          .then(data => console.log('shared'), error => console.log(error));
      } else {
        console.log('Something wired happen!')
      }
    }
  }
}
