import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import {FORM_DIRECTIVES} from '@angular/forms'

import {MD_BUTTON_DIRECTIVES} from '@angular2-material/button';
import {MD_INPUT_DIRECTIVES} from '@angular2-material/input';

@Component({
  moduleId: module.id,
  selector: 'app-search-fb-page',
  templateUrl: 'search-fb-page.component.html',
  styleUrls: ['search-fb-page.component.css'],
  directives: [
    FORM_DIRECTIVES,
    MD_BUTTON_DIRECTIVES,
    MD_INPUT_DIRECTIVES,
  ],
})
export class SearchFbPageComponent implements OnInit {

  @Output() searchEvent = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {
  }

  searchFired(search){
    this.searchEvent.emit(search);
  }
}
