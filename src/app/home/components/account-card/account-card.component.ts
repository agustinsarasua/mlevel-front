import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ROUTER_DIRECTIVES }  from '@angular/router';
import { MdButton } from '@angular2-material/button';
import { MdIcon, MdIconRegistry } from '@angular2-material/icon';
import { MD_CHECKBOX_DIRECTIVES } from '@angular2-material/checkbox';
import { MD_CARD_DIRECTIVES } from '@angular2-material/card';
import { Account } from './../../../model/facebook/account'

@Component({
  moduleId: module.id,
  selector: 'app-account-card',
  templateUrl: 'account-card.component.html',
  styleUrls: ['account-card.component.css'],
  directives: [
    MdButton,
    MdIcon,
    MD_CHECKBOX_DIRECTIVES,
    MD_CARD_DIRECTIVES,
    ROUTER_DIRECTIVES
  ],
})
export class AccountCardComponent {

  @Input('account') account: Account;

  @Output() shareEventClick = new EventEmitter<boolean>();

  constructor() {}

  checkboxChange(state){
    this.shareEventClick.emit(state.checked);
    this.account.share = state.checked;
  }

}
