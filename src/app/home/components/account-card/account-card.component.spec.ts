/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { AccountCardComponent } from './account-card.component';

// describe('Component: AccountCard', () => {
//   it('should create an instance', () => {
//     let component = new AccountCardComponent();
//     expect(component).toBeTruthy();
//   });
// });
