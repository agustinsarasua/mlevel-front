/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { DownloadButtonsComponent } from './download-buttons.component';

describe('Component: DownloadButtons', () => {
  it('should create an instance', () => {
    let component = new DownloadButtonsComponent();
    expect(component).toBeTruthy();
  });
});
