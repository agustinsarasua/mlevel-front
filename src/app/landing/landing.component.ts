import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {DownloadButtonsComponent} from "./components/download-buttons/download-buttons.component";
import {AngularFire} from "angularfire2";
import {AuthService} from "./../services/auth.service";

declare var FB: any;

@Component({
  moduleId: module.id,
  selector: 'app-landing',
  directives: [DownloadButtonsComponent, ROUTER_DIRECTIVES],
  templateUrl: 'landing.component.html',
  styleUrls: ['landing.component.css'],
})
export class LandingComponent {

  text:string;

  constructor(public af: AngularFire, private router: Router, private authService: AuthService) {
    this.af.auth.subscribe(auth => this.authenPimba(auth));
  }

  authenPimba(auth):any{
    if(auth != null) {
      localStorage.setItem("pimbaUserId", auth.uid);
      localStorage.setItem("pimbaToken", auth.auth.bd);
      this.authService.login();
      this.router.navigate(['/home']);
    }
  }
  login(){
    this.af.auth.login().then(data => {
      this.authenPimba(data);
    }, error => {
      console.log(error);
    });
  }
}
