export class Permission {
	public permission: string;
    public status: string;

    constructor(fbPerm: any){
	    this.permission = fbPerm.permission;
	    this.status = fbPerm.status;
  	}
}
