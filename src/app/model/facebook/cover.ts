export class Cover {
  public id: string;
  public source: string;
  public offsetX: number;
  public offsetY: number;

  constructor(cover: any){
    this.id = cover.id;
    this.source = cover.source;
    this.offsetX = cover.offsetX;
    this.offsetY = cover.offserY;
  }
}
