import {Cover} from "./cover"
import {Location} from "./location"

export class Account {
  public id: number;
  public facebookId: string;
  public about: string;
  public accessToken: string;
  public category: string;
  public categoryList: Category[];
  public fanCount: number;
  public isPublished: boolean;
  public link: string;
  public location: Location;
  public name: string;
  public talkingAboutCount: number;
  public website: string;
  public checkins: number;
  public cover: Cover;

  //Pimba fileds
  public share: boolean = false;

  constructor(account: any){
    this.facebookId = account.id;
    this.about = account.about;
    this.accessToken = account.access_token;
    this.category = account.category;
    this.categoryList = account.category_list;
    this.fanCount = account.fan_count;
    this.isPublished = account.is_published;
    this.link = account.link;
    this.location = account.location != null ? new Location(account.location) : null;
    this.name = account.name;
    this.talkingAboutCount = account.talking_about_count;
    this.website = account.website;
    this.checkins = account.checkins;
    this.cover = account.cover != null ? new Cover(account.cover) : null;
  }

}

export class Category {
  public id: string;
  public name: string;
}
