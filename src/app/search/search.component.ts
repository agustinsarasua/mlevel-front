import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ROUTER_DIRECTIVES } from '@angular/router';
import { Observable }        from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { MdToolbar } from '@angular2-material/toolbar';
import { MdButton } from '@angular2-material/button';
import { MD_LIST_DIRECTIVES } from '@angular2-material/list';
import { MD_PROGRESS_CIRCLE_DIRECTIVES } from '@angular2-material/progress-circle';
import { MdIcon, MdIconRegistry } from '@angular2-material/icon';

import { Account } from "./../model/facebook/account";
import { FacebookService } from "./../services/facebook.service";
import { AccountCardComponent } from "./../home/components/account-card/account-card.component";

@Component({
  moduleId: module.id,
  selector: 'app-search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.css'],
  directives: [
    MdButton,
    MD_LIST_DIRECTIVES,
    MD_PROGRESS_CIRCLE_DIRECTIVES,
    MdIcon,
    ROUTER_DIRECTIVES,
    AccountCardComponent
  ],
})
export class SearchComponent implements OnInit {

  private sub: any;
  type: string;
  query: string;
  isLoading: boolean = false;

  searchResults: Account[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private _fbService: FacebookService) {}

  ngOnInit() {
    this.isLoading = true;
    this.sub = this.route
      .params
      .subscribe(params => {
        this.query = params['q'];
        this.type = params['type'];
        this.facebookSearch(this.query);
      });
  }

  facebookSearch(searchQuery) {
    var accessToken = localStorage.getItem("facebookAccessToken");
    this._fbService.searchPages(searchQuery, accessToken).then(accounts =>
      {
        this.searchResults = accounts;
        this.isLoading = false;
      });
  }

}
