import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MdButton } from '@angular2-material/button';
import { MdIcon, MdIconRegistry } from '@angular2-material/icon';
import { ROUTER_DIRECTIVES }  from '@angular/router';
import { MD_CHECKBOX_DIRECTIVES } from '@angular2-material/checkbox';
import { MD_CARD_DIRECTIVES } from '@angular2-material/card';

@Component({
  moduleId: module.id,
  selector: 'app-card-item-a',
  templateUrl: 'card-item-a.component.html',
  styleUrls: ['card-item-a.component.css'],
  directives: [
    MdButton,
    MdIcon,
    MD_CHECKBOX_DIRECTIVES,
    MD_CARD_DIRECTIVES,
    ROUTER_DIRECTIVES
  ],
})
export class CardItemAComponent {

  @Input('title') title: string;
  @Input('description') description: string;
  @Input('img-url') imgUrl: string;
  @Input('action-txt') actionText: string;
  @Input('checked') checked: boolean = false;

  @Output() actionClick = new EventEmitter<boolean>();

  constructor() {}

  checkboxChange(state){
    this.actionClick.emit(state.checked);
    this.checked = state.checked;
  }
}
