/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { CardItemAComponent } from './card-item-a.component';

// describe('Component: CardItemA', () => {
//   it('should create an instance', () => {
//     let component = new CardItemAComponent();
//     expect(component).toBeTruthy();
//   });
// });
